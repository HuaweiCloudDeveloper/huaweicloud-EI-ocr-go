# 文字识别服务文字识别示例（go版本）
## 0.版本说明

本示例基于华为云SDK V3.0版本开发

## 1.简介

华为云提供了文字识别服务端SDK，您可以直接集成服务端SDK来调用文字识别服务的相关API，从而实现对文字识别服务的快速操作。

该示例展示了如何通过go版SDK实现文字识别。

## 2.开发前准备

- 已 [注册](https://reg.huaweicloud.com/registerui/cn/register.html?locale=zh-cn#/register) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth) 。
- 已订阅文字识别服务。
- 已具备开发环境，支持go 1.14及其以上版本。
- 已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- 已获取文字识别服务对应区域的项目ID，请在华为云控制台“我的凭证 > API凭证”页面上查看项目ID。具体请参见 [API凭证](https://support.huaweicloud.com/usermanual-ca/ca_01_0002.html) 。

## 3.环境配置
``` go
// 启用go module
export GO111MODULE=on

export GONOSUMDB=*

//若下载不了sdk，则需要设置源
export GOPROXY=https://repo.huaweicloud.com/repository/goproxy/,direct
```

## 4.安装SDK及代码依赖
``` go
// 安装华为云Go库
go get -u github.com/huaweicloud/huaweicloud-sdk-go-v3
// 安装依赖
go get github.com/json-iterator/go
go get github.com/huaweicloud/huaweicloud-sdk-go-v3/core/auth/signer@v0.1.2
go get github.com/huaweicloud/huaweicloud-sdk-go-v3/core/auth/provider@v0.1.2
go get github.com/huaweicloud/huaweicloud-sdk-go-v3/core/region@v0.1.2
```

## 5.开始使用

### 5.1 导入依赖模块
``` go
import (
	"fmt"
	"github.com/huaweicloud/huaweicloud-sdk-go-v3/core/auth/basic"
	ocr "github.com/huaweicloud/huaweicloud-sdk-go-v3/services/ocr/v1"
	"github.com/huaweicloud/huaweicloud-sdk-go-v3/services/ocr/v1/model"
	region "github.com/huaweicloud/huaweicloud-sdk-go-v3/services/ocr/v1/region"
)
```
### 5.2 初始化认证信息以及文字识别服务的客户端
``` go
ak := "<YOUR AK>"
sk := "<YOUR SK>"
regionName := "<YOUR REGION NAME>"
imageBase64 := "<YOUR IMAGE BASE64>"
// 初始化认证信息
auth := basic.NewCredentialsBuilder().
    WithAk(ak).
    WithSk(sk).
    Build()
// 获取服务调用client
client := ocr.NewOcrClient(
    ocr.OcrClientBuilder().
        WithRegion(region.ValueOf(regionName)).
        WithCredential(auth).
        Build())
```
相关参数说明如下
- ak：华为云账号Access Key。
- sk：华为云账号Secret Access Key 。

service region: 服务所在区域名称，例如：
- cn-north-1 北京一
- cn-north-4 北京四
- CN_EAST_3 上海一
- CN_SOUTH_1 华南广州

## 6.SDK demo代码解析

### 6.1 通用文字识别
``` go
request := &model.RecognizeGeneralTextRequest{}
quickModeGeneralTextRequestBody := true
detectDirectionGeneralTextRequestBody := true
imageGeneralTextRequestBody := imageBase64
request.Body = &model.GeneralTextRequestBody{
    QuickMode:       &quickModeGeneralTextRequestBody,
    DetectDirection: &detectDirectionGeneralTextRequestBody,
    Image:           &imageGeneralTextRequestBody,
}
response, err := client.RecognizeGeneralText(request)
if err == nil {
    fmt.Printf("%+v\n", response)
} else {
    fmt.Println(err)
}
```

### 6.2 通用表格识别
``` go
request := &model.RecognizeGeneralTableRequest{}
returnExcelGeneralTableRequestBody := true
returnConfidenceGeneralTableRequestBody := true
returnTextLocationGeneralTableRequestBody := true
imageGeneralTableRequestBody := imageBase64
request.Body = &model.GeneralTableRequestBody{
    ReturnExcel:        &returnExcelGeneralTableRequestBody,
    ReturnConfidence:   &returnConfidenceGeneralTableRequestBody,
    ReturnTextLocation: &returnTextLocationGeneralTableRequestBody,
    Image:              &imageGeneralTableRequestBody,
}
response, err := client.RecognizeGeneralTable(request)
if err == nil {
    fmt.Printf("%+v\n", response)
} else {
    fmt.Println(err)
}
```
### 6.3 身份证识别
``` go
request := &model.RecognizeIdCardRequest{}
returnVerificationIdCardRequestBody := true
sideIdCardRequestBody := "front"
imageIdCardRequestBody := imageBase64
request.Body = &model.IdCardRequestBody{
    ReturnVerification: &returnVerificationIdCardRequestBody,
    Side:               &sideIdCardRequestBody,
    Image:              &imageIdCardRequestBody,
}
response, err := client.RecognizeIdCard(request)
if err == nil {
    fmt.Printf("%+v\n", response)
} else {
    fmt.Println(err)
}

```
### 6.4 银行卡识别
``` go
request := &model.RecognizeBankcardRequest{}
imageBankcardRequestBody := imageBase64
request.Body = &model.BankcardRequestBody{
    Image: &imageBankcardRequestBody,
}
response, err := client.RecognizeBankcard(request)
if err == nil {
    fmt.Printf("%+v\n", response)
} else {
    fmt.Println(err)
}

```

### 6.5 智能分类识别
``` go
request := &model.RecognizeAutoClassificationRequest{}
imageAutoClassificationRequestBody := imageBase64
request.Body = &model.AutoClassificationRequestBody{
    Image: &imageAutoClassificationRequestBody,
}
response, err := client.RecognizeAutoClassification(request)
if err == nil {
    fmt.Printf("%+v\n", response)
} else {
    fmt.Println(err)
}

```

### 6.6 增值税发票识别
``` go
request := &model.RecognizeVatInvoiceRequest{}
advancedModeVatInvoiceRequestBody := true
imageVatInvoiceRequestBody := imageBase64
request.Body = &model.VatInvoiceRequestBody{
    AdvancedMode: &advancedModeVatInvoiceRequestBody,
    Image:        &imageVatInvoiceRequestBody,
}
response, err := client.RecognizeVatInvoice(request)
if err == nil {
    fmt.Printf("%+v\n", response)
} else {
    fmt.Println(err)
}

```
### 6.7 定额发票识别
``` go
request := &model.RecognizeQuotaInvoiceRequest{}
imageQuotaInvoiceRequestBody := imageBase64
request.Body = &model.QuotaInvoiceRequestBody{
    Image: &imageQuotaInvoiceRequestBody,
}
response, err := client.RecognizeQuotaInvoice(request)
if err == nil {
    fmt.Printf("%+v\n", response)
} else {
    fmt.Println(err)
}

```
### 6.8 手写文字识别
``` go
request := &model.RecognizeHandwritingRequest{}
detectDirectionHandwritingRequestBody := true
charSetHandwritingRequestBody := "digit"
quickModeHandwritingRequestBody := true
imageHandwritingRequestBody := imageBase64
request.Body = &model.HandwritingRequestBody{
    DetectDirection: &detectDirectionHandwritingRequestBody,
    CharSet:         &charSetHandwritingRequestBody,
    QuickMode:       &quickModeHandwritingRequestBody,
    Image:           &imageHandwritingRequestBody,
}
response, err := client.RecognizeHandwriting(request)
if err == nil {
    fmt.Printf("%+v\n", response)
} else {
    fmt.Println(err)
}

```
### 6.9 行驶证识别
``` go
request := &model.RecognizeVehicleLicenseRequest{}
returnIssuingAuthorityVehicleLicenseRequestBody := true
sideVehicleLicenseRequestBody := "front"
imageVehicleLicenseRequestBody := imageBase64
request.Body = &model.VehicleLicenseRequestBody{
    ReturnIssuingAuthority: &returnIssuingAuthorityVehicleLicenseRequestBody,
    Side:                   &sideVehicleLicenseRequestBody,
    Image:                  &imageVehicleLicenseRequestBody,
}
response, err := client.RecognizeVehicleLicense(request)
if err == nil {
    fmt.Printf("%+v\n", response)
} else {
    fmt.Println(err)
}

```
### 6.10 道路运输证识别
``` go
request := &model.RecognizeTransportationLicenseRequest{}
imageTransportationLicenseRequestBody := imageBase64
request.Body = &model.TransportationLicenseRequestBody{
    Image: &imageTransportationLicenseRequestBody,
}
response, err := client.RecognizeTransportationLicense(request)
if err == nil {
    fmt.Printf("%+v\n", response)
} else {
    fmt.Println(err)
}

```
### 6.11 出租车发票识别
``` go
request := &model.RecognizeTaxiInvoiceRequest{}
imageTaxiInvoiceRequestBody := imageBase64
request.Body = &model.TaxiInvoiceRequestBody{
    Image: &imageTaxiInvoiceRequestBody,
}
response, err := client.RecognizeTaxiInvoice(request)
if err == nil {
    fmt.Printf("%+v\n", response)
} else {
    fmt.Println(err)
}

```
### 6.12 车辆通行费发票识别
``` go
request := &model.RecognizeTollInvoiceRequest{}
imageTollInvoiceRequestBody := imageBase64
request.Body = &model.TollInvoiceRequestBody{
    Image: &imageTollInvoiceRequestBody,
}
response, err := client.RecognizeTollInvoice(request)
if err == nil {
    fmt.Printf("%+v\n", response)
} else {
    fmt.Println(err)
}

```
### 6.13 机动车销售发票识别
``` go
request := &model.RecognizeMvsInvoiceRequest{}
imageMvsInvoiceRequestBody := imageBase64
request.Body = &model.MvsInvoiceRequestBody{
    Image: &imageMvsInvoiceRequestBody,
}
response, err := client.RecognizeMvsInvoice(request)
if err == nil {
    fmt.Printf("%+v\n", response)
} else {
    fmt.Println(err)
}

```
### 6.14 车牌识别
``` go
request := &model.RecognizeLicensePlateRequest{}
imageLicensePlateRequestBody := imageBase64
request.Body = &model.LicensePlateRequestBody{
    Image: &imageLicensePlateRequestBody,
}
response, err := client.RecognizeLicensePlate(request)
if err == nil {
    fmt.Printf("%+v\n", response)
} else {
    fmt.Println(err)
}

```
### 6.15 飞机行程单识别
``` go
request := &model.RecognizeFlightItineraryRequest{}
imageFlightItineraryRequestBody := imageBase64
request.Body = &model.FlightItineraryRequestBody{
    Image: &imageFlightItineraryRequestBody,
}
response, err := client.RecognizeFlightItinerary(request)
if err == nil {
    fmt.Printf("%+v\n", response)
} else {
    fmt.Println(err)
}

```
### 6.16 营业执照识别
``` go
request := &model.RecognizeBusinessLicenseRequest{}
imageBusinessLicenseRequestBody := imageBase64
request.Body = &model.BusinessLicenseRequestBody{
    Image: &imageBusinessLicenseRequestBody,
}
response, err := client.RecognizeBusinessLicense(request)
if err == nil {
    fmt.Printf("%+v\n", response)
} else {
    fmt.Println(err)
}

```
### 6.17 网络图片识别
``` go
request := &model.RecognizeWebImageRequest{}
var listExtractTypebody = []string{
    "contact_info",
    "image_size",
}
detectDirectionWebImageRequestBody := true
imageWebImageRequestBody := imageBase64
request.Body = &model.WebImageRequestBody{
    ExtractType:     &listExtractTypebody,
    DetectDirection: &detectDirectionWebImageRequestBody,
    Image:           &imageWebImageRequestBody,
}
response, err := client.RecognizeWebImage(request)
if err == nil {
    fmt.Printf("%+v\n", response)
} else {
    fmt.Println(err)
}

```
### 6.18 驾驶证识别
``` go
request := &model.RecognizeDriverLicenseRequest{}
returnIssuingAuthorityDriverLicenseRequestBody := true
sideDriverLicenseRequestBody := "front"
imageDriverLicenseRequestBody := imageBase64
request.Body = &model.DriverLicenseRequestBody{
    ReturnIssuingAuthority: &returnIssuingAuthorityDriverLicenseRequestBody,
    Side:                   &sideDriverLicenseRequestBody,
    Image:                  &imageDriverLicenseRequestBody,
}
response, err := client.RecognizeDriverLicense(request)
if err == nil {
    fmt.Printf("%+v\n", response)
} else {
    fmt.Println(err)
}

```
### 6.19 名片识别
``` go
request := &model.RecognizeBusinessCardRequest{}
returnAdjustedImageBusinessCardRequestBody := true
detectDirectionBusinessCardRequestBody := true
imageBusinessCardRequestBody := imageBase64
request.Body = &model.BusinessCardRequestBody{
    ReturnAdjustedImage: &returnAdjustedImageBusinessCardRequestBody,
    DetectDirection:     &detectDirectionBusinessCardRequestBody,
    Image:               &imageBusinessCardRequestBody,
}
response, err := client.RecognizeBusinessCard(request)
if err == nil {
    fmt.Printf("%+v\n", response)
} else {
    fmt.Println(err)
}

```
### 6.20 火车票识别
``` go
request := &model.RecognizeTrainTicketRequest{}
imageTrainTicketRequestBody := imageBase64
request.Body = &model.TrainTicketRequestBody{
    Image: &imageTrainTicketRequestBody,
}
response, err := client.RecognizeTrainTicket(request)
if err == nil {
    fmt.Printf("%+v\n", response)
} else {
    fmt.Println(err)
}

```
### 6.21 VIN码识别
``` go
request := &model.RecognizeVinRequest{}
imageVinRequestBody := imageBase64
request.Body = &model.VinRequestBody{
    Image: &imageVinRequestBody,
}
response, err := client.RecognizeVin(request)
if err == nil {
    fmt.Printf("%+v\n", response)
} else {
    fmt.Println(err)
}

```
### 6.22 护照识别
``` go
request := &model.RecognizePassportRequest{}
imagePassportRequestBody := imageBase64
request.Body = &model.PassportRequestBody{
    Image: &imagePassportRequestBody,
}
response, err := client.RecognizePassport(request)
if err == nil {
    fmt.Printf("%+v\n", response)
} else {
    fmt.Println(err)
}

```
### 6.23 保险单卡识别
``` go
request := &model.RecognizeInsurancePolicyRequest{}
detectDirectionInsurancePolicyRequestBody := true
imageInsurancePolicyRequestBody := imageBase64
request.Body = &model.InsurancePolicyRequestBody{
    DetectDirection: &detectDirectionInsurancePolicyRequestBody,
    Image:           &imageInsurancePolicyRequestBody,
}
response, err := client.RecognizeInsurancePolicy(request)
if err == nil {
    fmt.Printf("%+v\n", response)
} else {
    fmt.Println(err)
}

```
### 6.24 道路运输从业资格证识别
``` go
request := &model.RecognizeQualificationCertificateRequest{}
imageQualificationCertificateRequestBody := imageBase64
request.Body = &model.QualificationCertificateRequestBody{
    Image: &imageQualificationCertificateRequestBody,
}
response, err := client.RecognizeQualificationCertificate(request)
if err == nil {
    fmt.Printf("%+v\n", response)
} else {
    fmt.Println(err)
}

```

## 7.参考
更多信息请参考[文字识别服务](https://support.huaweicloud.com/ocr/index.html)

## 8.修订记录

|    发布日期    | 文档版本 | 修订说明 |
|:----------:|:----:|:----:|
| 2022-10-10 | 2.0  | 文档发布 |