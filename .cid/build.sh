#!/bin/bash

PROJECT_ROOT=$(cd `dirname $0/`/..;pwd)

cd $PROJECT_ROOT/code

export GO111MODULE=on
export GONOSUMDB=*

go get -u github.com/huaweicloud/huaweicloud-sdk-go-v3
go get -u github.com/json-iterator/go
go get -u github.com/huaweicloud/huaweicloud-sdk-go-v3/core/auth/signer@v0.1.2
go get -u github.com/huaweicloud/huaweicloud-sdk-go-v3/core/auth/provider@v0.1.2
go get -u github.com/huaweicloud/huaweicloud-sdk-go-v3/core/region@v0.1.2
cd $PROJECT_ROOT/code/src/example
CGO_CFLAGS="-fstack-protector-strong -D_FORTIFY_SOURCE=2 -O2" go build -buildmode=pie --ldflags "-s -linkmode 'external' -extldflags '-Wl,-z,now'" main.go

cd $PROJECT_ROOT
tar -czvf $PROJECT_ROOT/huaweicloud-EI-ocr-go-${CID_BUILD_TIME}.tar.gz .codelabs code introduction
