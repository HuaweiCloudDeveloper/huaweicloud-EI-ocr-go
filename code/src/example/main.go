package main

import (
	"fmt"
	"github.com/huaweicloud/huaweicloud-sdk-go-v3/core/auth/basic"
	ocr "github.com/huaweicloud/huaweicloud-sdk-go-v3/services/ocr/v1"
	"github.com/huaweicloud/huaweicloud-sdk-go-v3/services/ocr/v1/model"
	region "github.com/huaweicloud/huaweicloud-sdk-go-v3/services/ocr/v1/region"
)

func main() {
	ak := "<YOUR AK>"
	sk := "<YOUR SK>"
	regionName := "<YOUR REGION NAME>"
	imageBase64 := "<YOUR IMAGE BASE64>"
	// 初始化认证信息
	auth := basic.NewCredentialsBuilder().
		WithAk(ak).
		WithSk(sk).
		Build()
	// 获取服务调用client
	client := ocr.NewOcrClient(
		ocr.OcrClientBuilder().
			WithRegion(region.ValueOf(regionName)).
			WithCredential(auth).
			Build())
	// 通用文字识别
	RecognizeGeneralText(client, imageBase64)
	// 通用表格识别
	RecognizeGeneralTable(client, imageBase64)
	// 身份证识别
	RecognizeIdCard(client, imageBase64)
	// 银行卡识别
	RecognizeBankcard(client, imageBase64)
	// 智能分类识别
	RecognizeAutoClassification(client, imageBase64)
	// 增值税发票识别
	RecognizeVatInvoice(client, imageBase64)
	// 定额发票识别
	RecognizeQuotaInvoice(client, imageBase64)
	// 手写文字识别
	RecognizeHandwriting(client, imageBase64)
	// 行驶证识别
	RecognizeVehicleLicense(client, imageBase64)
	// 道路运输证识别
	RecognizeTransportationLicense(client, imageBase64)
	// 出租车发票识别
	RecognizeTaxiInvoice(client, imageBase64)
	// 车辆通行费发票识别
	RecognizeTollInvoice(client, imageBase64)
	// 机动车销售发票识别
	RecognizeMvsInvoice(client, imageBase64)
	// 车牌识别
	RecognizeLicensePlate(client, imageBase64)
	// 飞机行程单识别
	RecognizeFlightItinerary(client, imageBase64)
	// 营业执照识别
	RecognizeBusinessLicense(client, imageBase64)
	// 网络图片识别
	RecognizeWebImage(client, imageBase64)
	// 驾驶证识别
	RecognizeDriverLicense(client, imageBase64)
	// 名片识别
	RecognizeBusinessCard(client, imageBase64)
	// 火车票识别
	RecognizeTrainTicket(client, imageBase64)
	// VIN码识别
	RecognizeVin(client, imageBase64)
	// 护照识别
	RecognizePassport(client, imageBase64)
	// 保险单识别
	RecognizeInsurancePolicy(client, imageBase64)
	// 道路运输从业资格证识别
	RecognizeQualificationCertificate(client, imageBase64)
}

func RecognizeGeneralTable(client *ocr.OcrClient, imageBase64 string) {
	request := &model.RecognizeGeneralTableRequest{}
	returnExcelGeneralTableRequestBody := true
	returnConfidenceGeneralTableRequestBody := true
	returnTextLocationGeneralTableRequestBody := true
	imageGeneralTableRequestBody := imageBase64
	request.Body = &model.GeneralTableRequestBody{
		ReturnExcel:        &returnExcelGeneralTableRequestBody,
		ReturnConfidence:   &returnConfidenceGeneralTableRequestBody,
		ReturnTextLocation: &returnTextLocationGeneralTableRequestBody,
		Image:              &imageGeneralTableRequestBody,
	}
	response, err := client.RecognizeGeneralTable(request)
	if err == nil {
		fmt.Printf("%+v\n", response)
	} else {
		fmt.Println(err)
	}
}

func RecognizeVatInvoice(client *ocr.OcrClient, imageBase64 string) {
	request := &model.RecognizeVatInvoiceRequest{}
	advancedModeVatInvoiceRequestBody := true
	imageVatInvoiceRequestBody := imageBase64
	request.Body = &model.VatInvoiceRequestBody{
		AdvancedMode: &advancedModeVatInvoiceRequestBody,
		Image:        &imageVatInvoiceRequestBody,
	}
	response, err := client.RecognizeVatInvoice(request)
	if err == nil {
		fmt.Printf("%+v\n", response)
	} else {
		fmt.Println(err)
	}
}

func RecognizeGeneralText(client *ocr.OcrClient, imageBase64 string) {
	request := &model.RecognizeGeneralTextRequest{}
	quickModeGeneralTextRequestBody := true
	detectDirectionGeneralTextRequestBody := true
	imageGeneralTextRequestBody := imageBase64
	request.Body = &model.GeneralTextRequestBody{
		QuickMode:       &quickModeGeneralTextRequestBody,
		DetectDirection: &detectDirectionGeneralTextRequestBody,
		Image:           &imageGeneralTextRequestBody,
	}
	response, err := client.RecognizeGeneralText(request)
	if err == nil {
		fmt.Printf("%+v\n", response)
	} else {
		fmt.Println(err)
	}
}

func RecognizeQuotaInvoice(client *ocr.OcrClient, imageBase64 string) {
	request := &model.RecognizeQuotaInvoiceRequest{}
	imageQuotaInvoiceRequestBody := imageBase64
	request.Body = &model.QuotaInvoiceRequestBody{
		Image: &imageQuotaInvoiceRequestBody,
	}
	response, err := client.RecognizeQuotaInvoice(request)
	if err == nil {
		fmt.Printf("%+v\n", response)
	} else {
		fmt.Println(err)
	}
}

func RecognizeIdCard(client *ocr.OcrClient, imageBase64 string) {
	request := &model.RecognizeIdCardRequest{}
	returnVerificationIdCardRequestBody := true
	sideIdCardRequestBody := "front"
	imageIdCardRequestBody := imageBase64
	request.Body = &model.IdCardRequestBody{
		ReturnVerification: &returnVerificationIdCardRequestBody,
		Side:               &sideIdCardRequestBody,
		Image:              &imageIdCardRequestBody,
	}
	response, err := client.RecognizeIdCard(request)
	if err == nil {
		fmt.Printf("%+v\n", response)
	} else {
		fmt.Println(err)
	}
}

func RecognizeHandwriting(client *ocr.OcrClient, imageBase64 string) {
	request := &model.RecognizeHandwritingRequest{}
	detectDirectionHandwritingRequestBody := true
	charSetHandwritingRequestBody := "digit"
	quickModeHandwritingRequestBody := true
	imageHandwritingRequestBody := imageBase64
	request.Body = &model.HandwritingRequestBody{
		DetectDirection: &detectDirectionHandwritingRequestBody,
		CharSet:         &charSetHandwritingRequestBody,
		QuickMode:       &quickModeHandwritingRequestBody,
		Image:           &imageHandwritingRequestBody,
	}
	response, err := client.RecognizeHandwriting(request)
	if err == nil {
		fmt.Printf("%+v\n", response)
	} else {
		fmt.Println(err)
	}
}

func RecognizeVehicleLicense(client *ocr.OcrClient, imageBase64 string) {
	request := &model.RecognizeVehicleLicenseRequest{}
	returnIssuingAuthorityVehicleLicenseRequestBody := true
	sideVehicleLicenseRequestBody := "front"
	imageVehicleLicenseRequestBody := imageBase64
	request.Body = &model.VehicleLicenseRequestBody{
		ReturnIssuingAuthority: &returnIssuingAuthorityVehicleLicenseRequestBody,
		Side:                   &sideVehicleLicenseRequestBody,
		Image:                  &imageVehicleLicenseRequestBody,
	}
	response, err := client.RecognizeVehicleLicense(request)
	if err == nil {
		fmt.Printf("%+v\n", response)
	} else {
		fmt.Println(err)
	}
}

func RecognizeTransportationLicense(client *ocr.OcrClient, imageBase64 string) {
	request := &model.RecognizeTransportationLicenseRequest{}
	imageTransportationLicenseRequestBody := imageBase64
	request.Body = &model.TransportationLicenseRequestBody{
		Image: &imageTransportationLicenseRequestBody,
	}
	response, err := client.RecognizeTransportationLicense(request)
	if err == nil {
		fmt.Printf("%+v\n", response)
	} else {
		fmt.Println(err)
	}
}

func RecognizeTaxiInvoice(client *ocr.OcrClient, imageBase64 string) {
	request := &model.RecognizeTaxiInvoiceRequest{}
	imageTaxiInvoiceRequestBody := imageBase64
	request.Body = &model.TaxiInvoiceRequestBody{
		Image: &imageTaxiInvoiceRequestBody,
	}
	response, err := client.RecognizeTaxiInvoice(request)
	if err == nil {
		fmt.Printf("%+v\n", response)
	} else {
		fmt.Println(err)
	}
}

func RecognizeAutoClassification(client *ocr.OcrClient, imageBase64 string) {
	request := &model.RecognizeAutoClassificationRequest{}
	imageAutoClassificationRequestBody := imageBase64
	request.Body = &model.AutoClassificationRequestBody{
		Image: &imageAutoClassificationRequestBody,
	}
	response, err := client.RecognizeAutoClassification(request)
	if err == nil {
		fmt.Printf("%+v\n", response)
	} else {
		fmt.Println(err)
	}
}

func RecognizeTollInvoice(client *ocr.OcrClient, imageBase64 string) {
	request := &model.RecognizeTollInvoiceRequest{}
	imageTollInvoiceRequestBody := imageBase64
	request.Body = &model.TollInvoiceRequestBody{
		Image: &imageTollInvoiceRequestBody,
	}
	response, err := client.RecognizeTollInvoice(request)
	if err == nil {
		fmt.Printf("%+v\n", response)
	} else {
		fmt.Println(err)
	}
}

func RecognizeMvsInvoice(client *ocr.OcrClient, imageBase64 string) {
	request := &model.RecognizeMvsInvoiceRequest{}
	imageMvsInvoiceRequestBody := imageBase64
	request.Body = &model.MvsInvoiceRequestBody{
		Image: &imageMvsInvoiceRequestBody,
	}
	response, err := client.RecognizeMvsInvoice(request)
	if err == nil {
		fmt.Printf("%+v\n", response)
	} else {
		fmt.Println(err)
	}
}

func RecognizeLicensePlate(client *ocr.OcrClient, imageBase64 string) {
	request := &model.RecognizeLicensePlateRequest{}
	imageLicensePlateRequestBody := imageBase64
	request.Body = &model.LicensePlateRequestBody{
		Image: &imageLicensePlateRequestBody,
	}
	response, err := client.RecognizeLicensePlate(request)
	if err == nil {
		fmt.Printf("%+v\n", response)
	} else {
		fmt.Println(err)
	}
}

func RecognizeFlightItinerary(client *ocr.OcrClient, imageBase64 string) {
	request := &model.RecognizeFlightItineraryRequest{}
	imageFlightItineraryRequestBody := imageBase64
	request.Body = &model.FlightItineraryRequestBody{
		Image: &imageFlightItineraryRequestBody,
	}
	response, err := client.RecognizeFlightItinerary(request)
	if err == nil {
		fmt.Printf("%+v\n", response)
	} else {
		fmt.Println(err)
	}
}

func RecognizeBusinessLicense(client *ocr.OcrClient, imageBase64 string) {
	request := &model.RecognizeBusinessLicenseRequest{}
	imageBusinessLicenseRequestBody := imageBase64
	request.Body = &model.BusinessLicenseRequestBody{
		Image: &imageBusinessLicenseRequestBody,
	}
	response, err := client.RecognizeBusinessLicense(request)
	if err == nil {
		fmt.Printf("%+v\n", response)
	} else {
		fmt.Println(err)
	}
}

func RecognizeWebImage(client *ocr.OcrClient, imageBase64 string) {
	request := &model.RecognizeWebImageRequest{}
	var listExtractTypebody = []string{
		"contact_info",
		"image_size",
	}
	detectDirectionWebImageRequestBody := true
	imageWebImageRequestBody := imageBase64
	request.Body = &model.WebImageRequestBody{
		ExtractType:     &listExtractTypebody,
		DetectDirection: &detectDirectionWebImageRequestBody,
		Image:           &imageWebImageRequestBody,
	}
	response, err := client.RecognizeWebImage(request)
	if err == nil {
		fmt.Printf("%+v\n", response)
	} else {
		fmt.Println(err)
	}
}

func RecognizeDriverLicense(client *ocr.OcrClient, imageBase64 string) {
	request := &model.RecognizeDriverLicenseRequest{}
	returnIssuingAuthorityDriverLicenseRequestBody := true
	sideDriverLicenseRequestBody := "front"
	imageDriverLicenseRequestBody := imageBase64
	request.Body = &model.DriverLicenseRequestBody{
		ReturnIssuingAuthority: &returnIssuingAuthorityDriverLicenseRequestBody,
		Side:                   &sideDriverLicenseRequestBody,
		Image:                  &imageDriverLicenseRequestBody,
	}
	response, err := client.RecognizeDriverLicense(request)
	if err == nil {
		fmt.Printf("%+v\n", response)
	} else {
		fmt.Println(err)
	}
}

func RecognizeBusinessCard(client *ocr.OcrClient, imageBase64 string) {
	request := &model.RecognizeBusinessCardRequest{}
	returnAdjustedImageBusinessCardRequestBody := true
	detectDirectionBusinessCardRequestBody := true
	imageBusinessCardRequestBody := imageBase64
	request.Body = &model.BusinessCardRequestBody{
		ReturnAdjustedImage: &returnAdjustedImageBusinessCardRequestBody,
		DetectDirection:     &detectDirectionBusinessCardRequestBody,
		Image:               &imageBusinessCardRequestBody,
	}
	response, err := client.RecognizeBusinessCard(request)
	if err == nil {
		fmt.Printf("%+v\n", response)
	} else {
		fmt.Println(err)
	}
}

func RecognizeTrainTicket(client *ocr.OcrClient, imageBase64 string) {
	request := &model.RecognizeTrainTicketRequest{}
	imageTrainTicketRequestBody := imageBase64
	request.Body = &model.TrainTicketRequestBody{
		Image: &imageTrainTicketRequestBody,
	}
	response, err := client.RecognizeTrainTicket(request)
	if err == nil {
		fmt.Printf("%+v\n", response)
	} else {
		fmt.Println(err)
	}
}

func RecognizeVin(client *ocr.OcrClient, imageBase64 string) {
	request := &model.RecognizeVinRequest{}
	imageVinRequestBody := imageBase64
	request.Body = &model.VinRequestBody{
		Image: &imageVinRequestBody,
	}
	response, err := client.RecognizeVin(request)
	if err == nil {
		fmt.Printf("%+v\n", response)
	} else {
		fmt.Println(err)
	}
}

func RecognizePassport(client *ocr.OcrClient, imageBase64 string) {
	request := &model.RecognizePassportRequest{}
	imagePassportRequestBody := imageBase64
	request.Body = &model.PassportRequestBody{
		Image: &imagePassportRequestBody,
	}
	response, err := client.RecognizePassport(request)
	if err == nil {
		fmt.Printf("%+v\n", response)
	} else {
		fmt.Println(err)
	}
}

func RecognizeBankcard(client *ocr.OcrClient, imageBase64 string) {
	request := &model.RecognizeBankcardRequest{}
	imageBankcardRequestBody := imageBase64
	request.Body = &model.BankcardRequestBody{
		Image: &imageBankcardRequestBody,
	}
	response, err := client.RecognizeBankcard(request)
	if err == nil {
		fmt.Printf("%+v\n", response)
	} else {
		fmt.Println(err)
	}
}

func RecognizeInsurancePolicy(client *ocr.OcrClient, imageBase64 string) {
	request := &model.RecognizeInsurancePolicyRequest{}
	detectDirectionInsurancePolicyRequestBody := true
	imageInsurancePolicyRequestBody := imageBase64
	request.Body = &model.InsurancePolicyRequestBody{
		DetectDirection: &detectDirectionInsurancePolicyRequestBody,
		Image:           &imageInsurancePolicyRequestBody,
	}
	response, err := client.RecognizeInsurancePolicy(request)
	if err == nil {
		fmt.Printf("%+v\n", response)
	} else {
		fmt.Println(err)
	}
}

func RecognizeQualificationCertificate(client *ocr.OcrClient, imageBase64 string) {
	request := &model.RecognizeQualificationCertificateRequest{}
	imageQualificationCertificateRequestBody := imageBase64
	request.Body = &model.QualificationCertificateRequestBody{
		Image: &imageQualificationCertificateRequestBody,
	}
	response, err := client.RecognizeQualificationCertificate(request)
	if err == nil {
		fmt.Printf("%+v\n", response)
	} else {
		fmt.Println(err)
	}
}
